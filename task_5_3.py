class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        merged = [0] * (len(nums1) + len(nums2))
        f1 = f2 = 0
        for k in range(len(nums1) + len(nums2)):
            if f1 != len(nums1) and (f2 == len(nums2) or nums1[f1] <= nums2[f2]):
             merged[k] = nums1[f1]
             f1 += 1
            else:
            
                merged[k] = nums2[f2]
                f2 += 1

        if len(merged) % 2 != 0:
            median = merged[len(merged)//2]
        else:
            median = (merged[len(merged)//2] + merged[(len(merged)//2)-1])/2
        return median