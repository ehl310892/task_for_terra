from sqlalchemy.orm import Session

from . import models, schemas

def get_name_by_gender(db: Session, gender: str, limit: int):
    return db.query(models.Name).filter(models.Name.gender == gender).limit(limit).all()

def get_names(db: Session):
    return db.query(models.Name).all()

def create_name(db: Session, name: schemas.Names):
    db_name = models.Name(name=name.name, gender=name.gender)
    db.add(db_name)
    db.commit()
    db.refresh(db_name)
    return db_name