from sqlalchemy import Column,  Integer, String


from .database import Base

class Name(Base):
    __tablename__ = 'names'

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    gender = Column(String, nullable=False)