from fastapi import FastAPI, HTTPException, Depends
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post("/names/", response_model=schemas.Names)
def create_names(name: schemas.Names, db: Session = Depends(get_db)):
    return crud.create_name(db=db, name=name)

@app.get("/names/", response_model=list[schemas.Names])
def read_names(db: Session = Depends(get_db)):
    names = crud.get_names(db=db)
    return names

@app.get("/", response_model=list[schemas.Names])
def read_name_by_gender(gender: str, limit: int, db: Session = Depends(get_db)):
    names_by_gender = crud.get_name_by_gender(db=db, gender=gender, limit=limit)
    return names_by_gender



