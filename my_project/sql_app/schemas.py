from pydantic import BaseModel

class Names(BaseModel):
    id: int
    name: str
    gender: str

    class Config:
        orm_mode = True
