from aiogram import types, Dispatcher
from utils.trello import Trello
import aiohttp
import config
import requests
from database.sqlite_db import Database

db = Database(db_name='data.db')
trello = Trello()


#@dp.message_handler(commands=['active_boards'])
async def process_my_desks_command(message: types.Message):
    if db.check_user(message.from_user.id, 'users'):
        if db.check_token(message.from_user.id, 'users') and db.check_api(
                message.from_user.id, 'users'):

            api_key = db.get_api_key(message.from_user.id)
            api_token = db.get_api_token(message.from_user.id)

            if api_key and api_token:
                print(api_key)
                try:
                    response_text = await trello.get_all_boards(
                        api_key, api_token)
                except Exception as e:
                    await message.edit_text(f"ошибка >>> {e}")

                text = ''
                for desk in response_text:
                    id_desk = desk.get('id')
                    name = desk.get('name')
                    url = desk.get('url')
                    text += f'\n[{name}]({url}): {id_desk}\n'

                await message.reply(f"ID моих досок:\n{text}",
                                    parse_mode="Markdown")
            else:
                await message.reply(
                    f"Выполни команду /trello_api и /trello_token, для добавления ключа и токена"
                )

    #Отправляем обратное сообщение с ответом на запрос


def register_handler_descs(dp: Dispatcher):
    dp.register_message_handler(process_my_desks_command,
                                commands=['active_boards'])
