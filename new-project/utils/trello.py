import aiohttp

import config


class Trello:

    def __init__(self):
        pass

    async def get_board_tasks(self, api_key, token, board_id):
        url = f"https://api.trello.com/1/boards/{board_id}/cards"
        query_string = {
            "key": api_key,
            "token": token,
            'dateTime': 'Europe/Moscow'
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=query_string) as response:

                if response.status == 200:
                    return await response.json()
                else:
                    raise Exception("Failed to get Trello tasks.")

    async def get_all_boards(self, api_key, token):
        url = 'https://api.trello.com/1/members/me/boards?fields=id,name,url'
        query_string = {
            "key": api_key,
            "token": token,
            'dateTime': 'Europe/Moscow'
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=query_string) as response:

                if response.status == 200:
                    return await response.json()
                else:
                    raise Exception("Failed to get Trello tasks.")

    async def get_all_lists(self, api_key, token, card_id):
        url = f'https://api.trello.com/1/cards/{card_id}/list'
        query_string = {
            "key": api_key,
            "token": token,
            'dateTime': 'Europe/Moscow'
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=query_string) as response:
                if response.status == 200:
                    return await response.json()
                else:
                    raise Exception("Failed to get Trello tasks.")

    async def get_labels(self, api_key, token, card_id):
        print(card_id)
        url = f"https://api.trello.com/1/cards/{card_id}/labels"
        query_string = {
            "key": api_key,
            "token": token,
            'dateTime': 'Europe/Moscow'
        }

        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=query_string) as response:
                print(response.status)
                if response.status == 200:
                    return await response.json()
                else:
                    raise Exception("Failed to get Trello tasks.")
